const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')
const path = require('path')


const app = express()
const jsonParser = bodyParser.json()

app.use('/assets', express.static(__dirname+'/assets'))
app.use('/assets2', express.static(__dirname+'/assets2'))
app.use('/css2', express.static(__dirname+'/css2'))
app.use('/css', express.static(__dirname+'/css'))
app.use('/js', express.static(__dirname+'/js'))

//GET POST PUT  DELETE
app.get('/', function (req, res) {
  let data = JSON.parse(fs.readFileSync('./user.json'))
  res.send(data)
})

app.get('/search', function (req, res) {
  let queryName = (req.query.name || "").toLowerCase()
  let queryAlamat = (req.query.alamat || "").toLowerCase()
  let data = JSON.parse(fs.readFileSync('./user.json', 'utf-8'))
  let filteredData = []

  //looping and filter 
  for(let i = 0; i<data.length; i++){
    if(data[i].name.toLowerCase().includes(queryName) && data[i].alamat.toLowerCase().includes(queryAlamat)){
      filteredData.push(data[i])
    }
  }

  if (filteredData != 0){
    res.send(filteredData)
  }
  else{
    res.status(404).send('Data Not Found')
  }
  
})

app.get('/challenge-3', function(req, res) {
  res.sendFile(path.join(__dirname + '/challenge-3.html'))
})

app.get('/challenge-4', function(req, res) {
  res.sendFile(path.join(__dirname + '/challenge-4.html'))
})

app.get('/:id', function (req, res) {
  let result

  let data = JSON.parse(fs.readFileSync('./user.json', 'utf-8'))
  // find result
  for(let i = 0; i<data.length; i++){
    if(data[i].id == parseInt(req.params.id)){
      result = data[i]
    }
  }
  
  if(result != undefined){
    res.send(result)
  }else{
    res.status(404).send("DATA NOT FOUND")
  }
})

app.post('/', jsonParser, (req, res) => {
  let data = JSON.parse(fs.readFileSync('./user.json', 'utf-8'))
  let newId = data[data.length -1].id + 1
  req.body.id = newId
  data.push(req.body)
  fs.writeFileSync('./user.json', JSON.stringify(data))
  res.status(201).send(data)
})

app.post('/login', jsonParser, (req, res) => {
  const PASSWORD = "123456"
  if(PASSWORD === req.body.password){
    res.send("Authorized")
  }else{
    res.status(401).send("Unauthorized")
  }
})


app.listen(3000)